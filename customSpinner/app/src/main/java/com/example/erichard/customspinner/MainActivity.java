package com.example.erichard.customspinner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity {

    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] strings = { "Sony", "Google", "Microsoft", "Apple", "Yahoo", "Samsung" };
        ArrayList<ListObject> objects = new ArrayList<>();

        for (String string : strings) {
            objects.add(new ListObject(string, "Sub title"));
        }

        Spinner spinner = findViewById(R.id.spinner);

        myAdapter = new MyAdapter(this, objects);
        spinner.setAdapter(myAdapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ListObject listObject = (ListObject) myAdapter.getItem(i);
                String company = String.format("Company :: %s", listObject.getCompany());
                Toast.makeText(getApplicationContext(), company, LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}

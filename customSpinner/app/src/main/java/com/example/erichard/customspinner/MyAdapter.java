package com.example.erichard.customspinner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {

    private ArrayList<ListObject> objects;
    private LayoutInflater layoutInflater;

    public MyAdapter(Activity activity, ArrayList<ListObject> objects) {
        this.objects = objects;
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int i) {
        return objects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint({"InflateParams", "ViewHolder"})
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ListObject curObj = objects.get(i);

        View row = layoutInflater.inflate(R.layout.row_spinner, null);

        TextView label = row.findViewById(R.id.company);
        label.setText(curObj.getCompany());

        TextView sub = row.findViewById(R.id.sub);
        sub.setText(curObj.getSub());

        return row;
    }
}

package com.example.erichard.customspinner;


public class ListObject {

    private String company;
    private String sub;

    public ListObject() {
    }

    public ListObject(String company, String sub) {
        this.company = company;
        this.sub = sub;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }
}
